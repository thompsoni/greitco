(function() {
  'use strict';

  angular
    .module('angularAssignment')
    .component('cardList', {
      templateUrl: 'app/cards/list/list.html',
      controller: CardListController,
      bindings: {
        contacts: '='
      }
    });

  /** @ngInject */
  function CardListController($scope) {

    $scope.contacts = this.contacts;
    $scope.contactsActive = [];

    for( var i=0; i<$scope.contacts.length; i++ )
    {
        if( $scope.contacts[i].isActive == true )
        {
            $scope.contactsActive.push( $scope.contacts[i] );
        }
    }

    console.log($scope.contactsActive);

  }
})();
