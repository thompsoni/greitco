(function() {
  'use strict';

  angular
    .module('angularAssignment')
    .config(cardsRouterConfig);

  /** @ngInject */
  function cardsRouterConfig($stateProvider) {
    $stateProvider

      .state('app.cards', {
        url: 'cards',
        views: {
          main: {
            template: '<card-list contacts="contacts"></card-list>',
            controller: function ($scope, contacts) {
              $scope.contacts = contacts;
            }
          }
        },
        resolve: {
          contactsApi: 'contactsApi',
          contacts: function(contactsApi) {
            console.log(contactsApi.query().$promise);
            return contactsApi.query().$promise;
          }
        }
      })

      .state('app.cards.view', {
        url: '/{id:int}',
        views: {
          'main@app': {
            template: '<card-list contacts="contacts"></card-list>',
            controller: function ($scope, contact) {
              $scope.contact = contact;
            }
          }
        },
        resolve: {
          /** @ngInject */
          contact: function($stateParams, contactsApi) {
            return contactsApi.get({ id: $stateParams.id }).$promise;
          }
        }
      });

      // .state('app.contacts.create', {
      //   url: '/create',
      //   views: {
      //     'main@app': {
      //       template: '<contact-form contact="contact"></contact-form>',
      //       controller: function ($scope, contact) {
      //         $scope.contact = contact;
      //       }
      //     }
      //   },
      //   resolve: {
      //     contact: function() {
      //       return { id: null, isActive: true }
      //     }
      //   }
      // })
      //
      // .state('app.contacts.edit', {
      //   url: '/{id:int}/edit',
      //   views: {
      //     'main@app': {
      //       template: '<contact-form contact="contact"></contact-form>',
      //       controller: function ($scope, contact) {
      //         $scope.contact = contact;
      //       }
      //     }
      //   },
      //   resolve: {
      //     /** @ngInject */
      //     contact: function($stateParams, contactsApi) {
      //       return contactsApi.get({ id: $stateParams.id }).$promise;
      //     }
      //   }
      // });

  }

})();
